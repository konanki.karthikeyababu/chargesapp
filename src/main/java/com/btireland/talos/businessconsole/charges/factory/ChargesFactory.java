package com.btireland.talos.businessconsole.charges.factory;


import com.btireland.talos.businessconsole.charges.service.impl.*;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChargesFactory {

    @Autowired
    private VoipChargesServiceImpl voipChargesService;

    @Autowired
    private WinChargesServiceImpl winChargesService;

    @Autowired
    private WlrChargeServiceImpl wlrChargeService;

    @Autowired
    private BroadbandChargesServiceImpl broadbandChargesService;

    @Autowired
    private WsiptChargesServiceImpl wsiptChargesService;
    public ChargesService getService(String sheetName){
        switch (sheetName){
            case "VOIP_CHARGES":
                return voipChargesService;
            case "WLR_CHARGES":
                return wlrChargeService;
            case "WIN_CHARGES":
                return winChargesService;
            case "BROADBAND_CHARGES":
                return broadbandChargesService;
            case "WSIPT_CHARGES":
                return wsiptChargesService;
            default:
                return null;
        }
    }
}
