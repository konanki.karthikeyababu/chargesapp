package com.btireland.talos.businessconsole.charges.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "broadband_charges_oao")
@Audited
@AuditTable(value = "BroadbandChargesOaoAudit")
public class BroadbandChargesOao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;

    @CreationTimestamp
    private Date created;

    @UpdateTimestamp
    private Date last_changed;

    private String oao;

    private String amount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bb_charge_id", referencedColumnName = "_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "_id")
    @JsonIdentityReference(alwaysAsId = true)
    private BroadbandCharges broadbandCharges;

}
