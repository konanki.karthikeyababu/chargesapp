package com.btireland.talos.businessconsole.charges.controller;

import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.service.impl.WsiptChargesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/wsipt")
public class WsiptChargesController {
    @Autowired
    private WsiptChargesServiceImpl wsiptChargesService;

    @GetMapping("/getById/{id}")
    public Optional<ChargesDto> getWsiptChargesByid(@PathVariable Integer id){
        return this.wsiptChargesService.getById(id);
    }

    @GetMapping("/getAll")
    public List<ChargesDto> getAllWsiptCharges(){
        return this.wsiptChargesService.getAll();
    }

    @PostMapping("/add")
    public Optional<ChargesDto> saveWsiptCharges(@RequestBody ChargesDto chargesDto){
        return this.wsiptChargesService.save(chargesDto);
    }

    @PostMapping("/addAll")
    public Optional<List<ChargesDto>> saveAllWsiptCharges(@RequestBody List<ChargesDto> chargesDtos){
        return this.wsiptChargesService.saveAll(chargesDtos);
    }

    @DeleteMapping("/delete/{id}")
    public  String deleteWsiptCharges(@PathVariable Integer id){
        return this.wsiptChargesService.deleteById(id);
    }

    @DeleteMapping("/deleteAll")
    public String deleleAllWsiptCharges(){
        return this.wsiptChargesService.deleteAllWsiptCharges();
    }

    @PutMapping("/update")
    public ChargesDto updateWsiptCharges(@RequestBody ChargesDto chargesDto){
        return this.wsiptChargesService.update(chargesDto);
    }

}
