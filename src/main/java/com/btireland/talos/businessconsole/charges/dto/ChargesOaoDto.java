package com.btireland.talos.businessconsole.charges.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChargesOaoDto {
    private int _id;
    private Date created;
    private Date last_changed;
    private String oao;
    private String amount;
}
