package com.btireland.talos.businessconsole.charges.service.impl;

import com.btireland.talos.businessconsole.charges.repository.VoipChargesRepository;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.entity.VoipCharges;
import com.btireland.talos.businessconsole.charges.entity.VoipChargesOao;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VoipChargesServiceImpl implements ChargesService {

    @Autowired
    private VoipChargesRepository voipChargesRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<VoipCharges> saveVoipCharges(List<VoipCharges> voipCharges) {
        return voipChargesRepository.saveAll(voipCharges);
    }

    public Optional<VoipCharges> getVoipCharge(int id){
        return voipChargesRepository.findById(id);
    }
    public VoipCharges saveVoipCharge(VoipCharges voipCharge) {
        List<VoipChargesOao> voipChargesOaos = voipCharge.getChargesOaoList();
        for(VoipChargesOao voipChargesOao : voipChargesOaos){
            voipChargesOao.setVoipCharges(voipCharge);
        }
        return voipChargesRepository.save(voipCharge);
    }


    public List<VoipCharges> getAllVoipcharges(){
        return voipChargesRepository.findAll();
    }

    public String deleteVoipCharges(int id) {
        voipChargesRepository.deleteById(id);
        return "product removed !! " + id;
    }

    public VoipCharges updateVoipCharge(VoipCharges voipCharges){
        List<VoipChargesOao> voipChargesOaos = voipCharges.getChargesOaoList();
        voipChargesOaos.forEach(voipChargesOao -> {
            voipChargesOao.setVoipCharges(voipCharges);
        });
        return voipChargesRepository.save(voipCharges);
    }

    @Override
    public List<ChargesDto> getAll() {
        List<ChargesDto> chargesDtos=new ArrayList<>();
        List<VoipCharges> voipChargesList=getAllVoipcharges();
//        System.out.println(voipChargesList.toString());
        for(VoipCharges voipCharges:voipChargesList){
            chargesDtos.add(modelMapper.map(voipCharges,ChargesDto.class));
        }
        return chargesDtos;
    }

    @Override
    public Optional<ChargesDto> getById(int id) {
        Optional<VoipCharges> voipCharges=getVoipCharge(id);
        return Optional.ofNullable(modelMapper.map(voipCharges.get(),ChargesDto.class));
    }

    @Override
    public Optional<List<ChargesDto>> saveAll(List<ChargesDto> list) {
        List<VoipCharges> voipCharges=new ArrayList<>();
        for(ChargesDto chargesDto:list){
            voipCharges.add(modelMapper.map(chargesDto,VoipCharges.class));
        }
        voipCharges=saveVoipCharges(voipCharges);
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for (VoipCharges v:voipCharges){
            chargesDtos.add(modelMapper.map(v,ChargesDto.class));
        }
        return Optional.ofNullable(chargesDtos);
    }

    @Override
    public Optional<ChargesDto> save(ChargesDto chargesDto) {
        VoipCharges voipCharges=modelMapper.map(chargesDto,VoipCharges.class);
        return Optional.ofNullable(modelMapper.map(saveVoipCharge(voipCharges), ChargesDto.class));
    }

    @Override
    public ChargesDto update(ChargesDto chargesDto) {
        VoipCharges voipCharges=modelMapper.map(chargesDto,VoipCharges.class);
        return modelMapper.map(updateVoipCharge(voipCharges),ChargesDto.class);
    }

    @Override
    public String deleteById(int id) {
        return deleteVoipCharges(id);
    }
}
