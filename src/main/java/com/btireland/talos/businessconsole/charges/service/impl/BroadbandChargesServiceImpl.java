package com.btireland.talos.businessconsole.charges.service.impl;

import com.btireland.talos.businessconsole.charges.entity.BroadbandChargesOao;
import com.btireland.talos.businessconsole.charges.entity.VoipCharges;
import com.btireland.talos.businessconsole.charges.entity.VoipChargesOao;
import com.btireland.talos.businessconsole.charges.repository.BroadbandChargesRepository;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.entity.BroadbandCharges;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BroadbandChargesServiceImpl implements ChargesService {

    @Autowired
    private BroadbandChargesRepository broadbandChargesRepository;
    @Autowired
    private ModelMapper modelMapper;


    public Optional<ChargesDto> saveBroadbandCharge(ChargesDto chargesDto) {
        BroadbandCharges broadbandCharges=modelMapper.map(chargesDto,BroadbandCharges.class);
        if(broadbandCharges.getChargesOaoList() != null) {
            List<BroadbandChargesOao> broadbandChargesOaoList = broadbandCharges.getChargesOaoList();
            for (BroadbandChargesOao eachBroadbandChargesOao : broadbandChargesOaoList) {
                eachBroadbandChargesOao.setBroadbandCharges(broadbandCharges);
            }
            broadbandCharges.setChargesOaoList(broadbandChargesOaoList);
        }
        BroadbandCharges broadbandCharges1=broadbandChargesRepository.save(broadbandCharges);

        return Optional.ofNullable(modelMapper.map(broadbandCharges1, ChargesDto.class));
    }

    public Optional<List<ChargesDto>> saveAllBroadbandCharge (List<ChargesDto> chargesDtoList) {
        List<BroadbandCharges> broadbandChargesList=new ArrayList<>();
        for(ChargesDto chargesDto:chargesDtoList){
            broadbandChargesList.add(modelMapper.map(chargesDto,BroadbandCharges.class));
        }
        List<BroadbandCharges> broadbandChargesList1=new ArrayList<>();
        for(BroadbandCharges currentBroadbandCharges : broadbandChargesList) {
            if (currentBroadbandCharges.getChargesOaoList() != null) {
                List<BroadbandChargesOao> broadbandChargesOaoList = currentBroadbandCharges.getChargesOaoList();
                for (BroadbandChargesOao eachBroadbandChargesOao : broadbandChargesOaoList) {
                    eachBroadbandChargesOao.setBroadbandCharges(currentBroadbandCharges);
                }
                currentBroadbandCharges.setChargesOaoList(broadbandChargesOaoList);
            }
            broadbandChargesList1.add(broadbandChargesRepository.save(currentBroadbandCharges));
        }
        chargesDtoList=new ArrayList<>();
        for(BroadbandCharges b:broadbandChargesList1){
            chargesDtoList.add(modelMapper.map(b,ChargesDto.class));
        }
        return Optional.of(chargesDtoList);
    }

    public List<ChargesDto> getAllBroadbandCharges() {
        List<ChargesDto> chargesDtos=new ArrayList<>();
        List<BroadbandCharges> broadbandChargesList=broadbandChargesRepository.findAll();
        for(BroadbandCharges broadbandCharges:broadbandChargesList){
            chargesDtos.add(modelMapper.map(broadbandCharges,ChargesDto.class));
        }
        return chargesDtos;
    }

    public ChargesDto getBroadbandChargesById(int id) {
        return modelMapper.map(broadbandChargesRepository.findById(id).orElse(null),ChargesDto.class);
    }
    public BroadbandCharges updateBroadbandCharges(BroadbandCharges broadbandCharges){
        List<BroadbandChargesOao> broadbandChargesOaos = broadbandCharges.getChargesOaoList();
        broadbandChargesOaos.forEach(broadbandChargesOao -> {
            broadbandChargesOao.setBroadbandCharges(broadbandCharges);
        });
        return broadbandChargesRepository.save(broadbandCharges);
    }
    public String deleteBroadbandCharge(int id) {
        broadbandChargesRepository.deleteById(id);
        return "broadbandCharges removed !! " + id;
    }

    @Override
    public List<ChargesDto> getAll() {
        return getAllBroadbandCharges();
    }

    @Override
    public Optional<ChargesDto> getById(int id) {
        return Optional.ofNullable(getBroadbandChargesById(id));
    }

    @Override
    public Optional<List<ChargesDto>> saveAll(List<ChargesDto> list) {
        return saveAllBroadbandCharge(list);
    }

    @Override
    public Optional<ChargesDto> save(ChargesDto chargesDto) {
        return saveBroadbandCharge(chargesDto);
    }

    @Override
    public ChargesDto update(ChargesDto chargesDto) {
        BroadbandCharges broadbandCharges=modelMapper.map(chargesDto,BroadbandCharges.class);
        return modelMapper.map(updateBroadbandCharges(broadbandCharges),ChargesDto.class);
    }

    @Override
    public String deleteById(int id) {
        return deleteBroadbandCharge(id);
    }
}
