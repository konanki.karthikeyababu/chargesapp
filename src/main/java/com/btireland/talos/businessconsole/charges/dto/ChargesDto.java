package com.btireland.talos.businessconsole.charges.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChargesDto {
    private int _id;
    private Date created;
    private Date last_changed;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;
    private String amount;
    private String discount;
    private List<ChargesOaoDto> chargesOaoList;
//    private List<AuditChargesDto>
}
