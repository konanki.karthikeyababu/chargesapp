package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.WinChargesOao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WinChargesAoaRepository extends JpaRepository<WinChargesOao,Integer> {
}
