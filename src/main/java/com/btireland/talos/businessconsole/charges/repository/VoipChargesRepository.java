package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.VoipCharges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoipChargesRepository extends JpaRepository<VoipCharges,Integer> {
}
