package com.btireland.talos.businessconsole.charges.controller;

import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.service.impl.WlrChargeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/wlr")
public class WlrChargesController {

    @Autowired
    private WlrChargeServiceImpl wlrChargeService;


    @GetMapping("/getAll")
    public List<ChargesDto> getWlrCharges(){
        return wlrChargeService.getAll();
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<Optional<ChargesDto>> getWlrCharge(@PathVariable int id){
        Optional<ChargesDto> chargesDto=wlrChargeService.getById(id);
        if(chargesDto.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(chargesDto,HttpStatus.OK);
    }

    @PostMapping("/add")
    public Optional<ChargesDto> addWlrCharge(@RequestBody ChargesDto chargesDto) {
        return wlrChargeService.save(chargesDto);
    }

    @PostMapping("/addAll")
    public Optional<List<ChargesDto>> addWlrCharges(@RequestBody List<ChargesDto> chargesDtos) {
        return wlrChargeService.saveAll(chargesDtos);
    }


    @PutMapping("/update")
    public ChargesDto updateWlrCharge(@RequestBody ChargesDto chargesDto){
        return wlrChargeService.update(chargesDto);
    }


    @DeleteMapping("/delete/{id}")
    public String deleteWlrCharge(@PathVariable int id) {
        return wlrChargeService.deleteById(id);
    }

}
