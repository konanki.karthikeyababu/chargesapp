package com.btireland.talos.businessconsole.charges.views;

import com.btireland.talos.businessconsole.charges.factory.ChargesFactory;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.views.entity.ChargesObj;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.dto.ChargesOaoDto;
import com.btireland.talos.businessconsole.charges.views.generic.PageableComponent;
import com.vaadin.flow.component.Focusable;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.AbstractListDataView;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.vaadin.flow.component.Component;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;


@PageTitle("Charges")
@Route(value = "charges", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class ChargesTable extends Div implements AfterNavigationObserver {
    @Autowired
    private ChargesFactory chargesFactory;
    private Grid<ChargesObj> chargesGrid = new Grid<>();
    private TextField actionField = new TextField();
    private TextField serviceField = new TextField();
    private TextField amountField = new TextField();
    private TextField oaoAmountField = new TextField();
    private TextField newAssetField = new TextField("Asset");
    private TextField newTypeField = new TextField("Type");
    private TextField newChargeField = new TextField("Charge");
    private TextField newNotificationField = new TextField("Notification");
    private TextField newRecordItemField = new TextField("Record item");
    private TextField newDescriptionField = new TextField("Description");
    private TextField newServiceField = new TextField("Service");
    private TextField newActionField = new TextField("Action");
    private TextField newAmountField = new TextField("Amount");
    private TextField newOaoField = new TextField("Oao");
    private TextField newOaoAmountField = new TextField("Oao amount");
    private Button addButton = new Button("Add Charges", e -> addButtonHandler(true));
    private Button saveButton = new Button("Save");
    private Button cancelButton = new Button("Cancel", e -> cancelButtonHandler(false));
    private Binder<ChargesObj> chargesBinder = new Binder<>(ChargesObj.class);
    private GridListDataView<ChargesObj> emptyDataView = chargesGrid.setItems(new ArrayList<ChargesObj>());
    private List<ChargesDto> chargesList = new ArrayList<ChargesDto>();
    private List<ChargesObj> chargesObjList = new ArrayList<ChargesObj>();
    private ChargesFilter chargesFilter = new ChargesFilter(emptyDataView);
    private Grid.Column<ChargesObj> assetColumn;
    private Grid.Column<ChargesObj> typeColumn;
    private Grid.Column<ChargesObj> chargeColumn;
    private Grid.Column<ChargesObj> notificationColumn;
    private Grid.Column<ChargesObj> record_itemColumn;
    private Grid.Column<ChargesObj> descriptionColumn;
    private Grid.Column<ChargesObj> serviceColumn;
    private Grid.Column<ChargesObj> actionColumn;
    private Grid.Column<ChargesObj> oaoColumn;
    private Grid.Column<ChargesObj> oaoAmountColumn;
    private Grid.Column<ChargesObj> amountColumn;
    private Grid.Column<ChargesObj> deleteColumn;
    private HeaderRow headerRow;
    private ChargesService chargesService;
    private String selectedValue;
    private List<TextField> listOfFormTextFields = Arrays.asList(newAssetField, newTypeField, newChargeField, newNotificationField, newRecordItemField, newDescriptionField, newServiceField, newActionField, newAmountField, newOaoField, newOaoAmountField);
    private String userRole = null;
    private HorizontalLayout gridAddRowButtons;
    private List<String> listOfFilterDropDownOptions;
    private final PageableComponent pageableComponent = new PageableComponent();
    private int currentPage = 0;
    private int totalPages;
    private int noOfRowsPerPage = 10;


    public ChargesTable(){
        chargesGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        chargesGrid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);

        chargesGrid.addColumn(ChargesObj::get_id).setHeader("ID");
        assetColumn = chargesGrid.addColumn(ChargesObj::getAsset).setHeader("Asset").setAutoWidth(true);
        typeColumn = chargesGrid.addColumn(ChargesObj::getType).setHeader("Type").setAutoWidth(true);
        chargeColumn = chargesGrid.addColumn(ChargesObj::getCharge).setHeader("Charge").setAutoWidth(true);
        notificationColumn = chargesGrid.addColumn(ChargesObj::getNotification).setHeader("Notification").setAutoWidth(true);
        record_itemColumn = chargesGrid.addColumn(ChargesObj::getRecord_item).setHeader("Record item").setAutoWidth(true);
        descriptionColumn = chargesGrid.addColumn(ChargesObj::getDescription).setHeader("Description").setAutoWidth(true);
        serviceColumn = chargesGrid.addColumn(ChargesObj::getService).setHeader("Service").setAutoWidth(true);
        actionColumn = chargesGrid.addColumn(ChargesObj::getAction).setHeader("Action").setAutoWidth(true);
        amountColumn = chargesGrid.addColumn(ChargesObj::getAmount).setHeader("Amount").setAutoWidth(true);
        oaoColumn = chargesGrid.addColumn(ChargesObj::getOao).setHeader("Oao").setAutoWidth(true);
        oaoAmountColumn = chargesGrid.addColumn(ChargesObj::getChild_amount).setHeader("Oao Amount").setAutoWidth(true);
        deleteColumn = chargesGrid.addComponentColumn(item -> {
            Button deleteButton = new Button("Delete");
            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
            deleteButton.addClickListener(e -> {
                chargesObjList.remove(item);

                chargesService.deleteById(item.get_id());

                chargesGrid.getDataProvider().refreshAll();
            });

            HorizontalLayout deleteButtonContainer = new HorizontalLayout(deleteButton);
            deleteButtonContainer.setPadding(false);
            return deleteButtonContainer;
        }).setAutoWidth(true);

//        chargesGrid.setTooltipGenerator(item -> "Last changed: "+item.getLastChanged()+" | Created: "+item.getCreated());
        chargesGrid.addColumn(createToggleDetailsRenderer(chargesGrid));
        chargesGrid.setDetailsVisibleOnClick(false);
        chargesGrid.setItemDetailsRenderer(createChargesDtoDetailsRenderer());

        chargesGrid.getStyle().set("margin", "30px");
        chargesGrid.setHeight("650px");

        VerticalLayout formLayout = new VerticalLayout();
        HorizontalLayout controlBtns = new HorizontalLayout();
        VerticalLayout formFieldsLayoutContainer = new VerticalLayout();
        HorizontalLayout formFieldsLayout1 = new HorizontalLayout();
        HorizontalLayout formFieldsLayout2 = new HorizontalLayout();
        HorizontalLayout formFieldsLayout3 = new HorizontalLayout();

        controlBtns.add(saveButton, cancelButton);
        formFieldsLayout1.add(newAssetField, newTypeField, newChargeField, newNotificationField);
        formFieldsLayout2.add(newRecordItemField, newDescriptionField, newServiceField, newActionField);
        formFieldsLayout3.add(newAmountField, newOaoField, newOaoAmountField);
        formFieldsLayoutContainer.add(formFieldsLayout1, formFieldsLayout2, formFieldsLayout3);
        formFieldsLayoutContainer.getStyle().set("padding-left", "0px");
        formLayout.add(formFieldsLayoutContainer, controlBtns);
        formLayout.getStyle().set("padding", "0px");
        formLayout.getStyle().set("margin", "0px 0px 0px 0px");
        addButton.getStyle().set("margin", "40px 0px -20px 0px").set("width", "200px");
        addButton.setEnabled(false);
        setFormVisibility(false, listOfFormTextFields);

        headerRow = chargesGrid.appendHeaderRow();

        actionField.setWidthFull();
        actionColumn.setEditorComponent(actionField);
        serviceColumn.setEditorComponent(serviceField);
        amountColumn.setEditorComponent(amountField);
        oaoAmountColumn.setEditorComponent(oaoAmountField);


        gridAddRowButtons = new HorizontalLayout(addButton, formLayout);
        gridAddRowButtons.getStyle().set("margin", "7px 0 0 30px");
        VerticalLayout pageableComponentContainer = new VerticalLayout(pageableComponent);
        pageableComponentContainer.setAlignItems(FlexComponent.Alignment.CENTER);
        add(gridAddRowButtons, chargesGrid, pageableComponentContainer);

        //configure form
        Editor<ChargesObj> chargesEditor = chargesGrid.getEditor();
        chargesEditor.setBinder(chargesBinder);

        chargesBinder.setBean(new ChargesObj());


        //edit field config

        chargesBinder.forField(actionField)
                .asRequired("Action must not be empty")
                .bind(ChargesObj::getAction, ChargesObj::setAction);

        chargesBinder.forField(serviceField)
                .asRequired("Service must not be empty")
                .bind(ChargesObj::getService, ChargesObj::setService);

        chargesBinder.forField(amountField)
                .asRequired("Amount must not be empty")
                .bind(ChargesObj::getAmount, ChargesObj::setAmount);

        chargesBinder.forField(oaoAmountField)
                .asRequired("Oao Amount must not be empty")
                .bind(ChargesObj::getChild_amount, ChargesObj::setChild_amount);

        chargesGrid.addItemDoubleClickListener(e -> {
            chargesEditor.editItem(e.getItem());
            Component editorComponent = e.getColumn().getEditorComponent();

            if (editorComponent instanceof Focusable) {
                ((Focusable) editorComponent).focus();
            }
        });

        // the grid valueChangeEvent will clear the form too
        List<TextField> listOfTextFields = new ArrayList<TextField>(Arrays.asList(
                actionField,
                serviceField,
                amountField,
                oaoAmountField
        ));
        addEditEventListners(chargesEditor, listOfTextFields);

        saveButton.addClickListener(e -> {
            setFormVisibility(false, listOfFormTextFields);

            String assetValue = newAssetField.getValue();
            String typeValue = newTypeField.getValue();
            String chargeValue = newChargeField.getValue();
            String notificationValue = newNotificationField.getValue();
            String recordItemValue = newRecordItemField.getValue();
            String descriptionValue = newDescriptionField.getValue();
            String serviceValue = newServiceField.getValue();
            String actionValue = newActionField.getValue();
            String amountValue = newAmountField.getValue();
            String oaoValue = newOaoField.getValue();
            String oaoAmountValue = newOaoAmountField.getValue();


            ChargesObj charges = new ChargesObj();
            charges.setAsset(assetValue);
            charges.setType(typeValue);
            charges.setCharge(chargeValue);
            charges.setNotification(notificationValue);
            charges.setRecord_item(recordItemValue);
            charges.setDescription(descriptionValue);
            charges.setService(serviceValue);
            charges.setAction(actionValue);
            charges.setAmount(amountValue);
            charges.setOao(oaoValue);
            charges.setChild_amount(oaoAmountValue);

            ChargesDto chargesDto = new ChargesDto();
            ChargesOaoDto chargesOaoDto = new ChargesOaoDto();

            chargesDto.setAsset(charges.getAsset());
            chargesDto.setType(charges.getType());
            chargesDto.setCharge(charges.getCharge());
            chargesDto.setNotification(charges.getNotification());
            chargesDto.setRecord_item(charges.getRecord_item());
            chargesDto.setDescription(charges.getDescription());
            chargesDto.setService(charges.getService());
            chargesDto.setAction(charges.getAction());
            chargesDto.setAmount(charges.getAmount());

            if(
                    charges.getChild_amount() != null ||
                            charges.getOao() != null
            ){
                chargesOaoDto.setAmount(charges.getChild_amount());
                chargesOaoDto.setOao(charges.getOao());
                List<ChargesOaoDto> chargesOaoDtoList = new ArrayList<ChargesOaoDto>(
                        Arrays.asList(chargesOaoDto)
                );
                chargesDto.setChargesOaoList(chargesOaoDtoList);
            } else {
                chargesDto.setChargesOaoList(new ArrayList<>());
            }

            Optional<ChargesDto> response = chargesService.save(chargesDto);

            charges.set_id(response.get().get_id());
            charges.setCreated(response.get().getCreated());
            chargesDto.set_id(response.get().get_id());
            chargesDto.setCreated((response.get().getCreated()));

            if(response.get().getChargesOaoList().size() > 0){
                charges.setChild_id(response.get().getChargesOaoList().get(0).get_id());
            }

            chargesObjList.add(charges);
            chargesList.add(chargesDto);

            chargesGrid.getDataProvider().refreshAll();

            clearInputFields(listOfFormTextFields);
        });
        configurePageableComponent();
    }
    private void configurePageableComponent() {
        pageableComponent.addListener(PageableComponent.PageChangeEvent.class, e -> {
            currentPage = e.getPageNumber();
            pageViewWithPagination();
            pageableComponent.configurePaging(totalPages, currentPage);
        });
    }

    private void pageViewWithPagination(){
        int startingIndex = currentPage * noOfRowsPerPage;
        int endIndex = Math.min(startingIndex + noOfRowsPerPage, chargesObjList.size());

        List<ChargesObj> paginationChargeObjList = chargesObjList.subList(startingIndex, endIndex);
        getDataView(paginationChargeObjList);
    }
    private static Renderer<ChargesObj> createToggleDetailsRenderer(Grid<ChargesObj> grid) {
        return LitRenderer.<ChargesObj> of(
                        "<vaadin-button theme=\"tertiary\" @click=\"${handleClick}\">details</vaadin-button>")
                .withFunction("handleClick",
                        chargesObj -> grid.setDetailsVisible(chargesObj,
                                !grid.isDetailsVisible(chargesObj)));
    }

    private static ComponentRenderer<ChargesDetailsFormLayout, ChargesObj> createChargesDtoDetailsRenderer() {
        return new ComponentRenderer<>(ChargesDetailsFormLayout::new, ChargesDetailsFormLayout::setChargesDto);
    }

    private void setFilterHeaders(List<String> listOfDropDownOptions){
        headerRow.getCell(assetColumn).setComponent(
                createFilterHeader(chargesFilter::setAsset, listOfDropDownOptions, "combobox"));
        headerRow.getCell(typeColumn).setComponent(
                createFilterHeader(chargesFilter::setType, listOfDropDownOptions, "combobox"));
        headerRow.getCell(chargeColumn).setComponent(
                createFilterHeader(chargesFilter::setCharge, listOfDropDownOptions, "combobox"));
        headerRow.getCell(notificationColumn).setComponent(
                createFilterHeader(chargesFilter::setNotification, listOfDropDownOptions, "combobox"));
        headerRow.getCell(record_itemColumn).setComponent(
                createFilterHeader(chargesFilter::setRecord_item, listOfDropDownOptions, "combobox"));
        headerRow.getCell(descriptionColumn).setComponent(
                createFilterHeader(chargesFilter::setDescription, listOfDropDownOptions, "combobox"));
        headerRow.getCell(serviceColumn).setComponent(
                createFilterHeader(chargesFilter::setService, listOfDropDownOptions, "combobox"));
        headerRow.getCell(actionColumn).setComponent(
                createFilterHeader(chargesFilter::setAction, listOfDropDownOptions, "combobox"));
        headerRow.getCell(amountColumn).setComponent(
                createFilterHeader(chargesFilter::setAmount, listOfDropDownOptions, "text"));
        headerRow.getCell(oaoColumn).setComponent(
                createFilterHeader(chargesFilter::setOao, listOfDropDownOptions, "combobox"));
        headerRow.getCell(oaoAmountColumn).setComponent(
                createFilterHeader(chargesFilter::setOaoAmount, listOfDropDownOptions, "text"));
    }

    private void addButtonHandler(boolean status){
        setFormVisibility(true, listOfFormTextFields);
    }

    private void cancelButtonHandler(boolean status){
        setFormVisibility(false, listOfFormTextFields);

        clearInputFields(listOfFormTextFields);
    }
    private void setFormVisibility(boolean status, List<TextField> listOfNewTextFields){
        saveButton.setVisible(status);
        cancelButton.setVisible(status);
        addButton.setVisible(!status);

        listOfNewTextFields.forEach(textField -> textField.setVisible(status));
    }

    private void clearInputFields(List<TextField> listOfNewTextFields){
        listOfNewTextFields.forEach(textField -> textField.clear());
    }

    private Component createFilterHeader(Consumer<String> filterChangeConsumer, List<String> listOfDropDownOptions, String fieldType) {
        if(fieldType.equals("combobox")){
            ComboBox<String> comboBoxField = new ComboBox();
            comboBoxField.setItems(listOfDropDownOptions);
            comboBoxField.setClearButtonVisible(true);
            comboBoxField.setWidthFull();
            comboBoxField.getStyle().set("width", "100%");
            comboBoxField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
            VerticalLayout layout = new VerticalLayout();
            layout.add(comboBoxField);
            layout.getThemeList().clear();
            layout.getThemeList().add("spacing-xs");

            return layout;
        }else {
            TextField textField = new TextField();
            textField.setValueChangeMode(ValueChangeMode.EAGER);
            textField.setClearButtonVisible(true);
            textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
            textField.setWidthFull();
            textField.getStyle().set("max-width", "100%");
            textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
            VerticalLayout layout = new VerticalLayout();
            layout.add(textField);
            layout.getThemeList().clear();
            layout.getThemeList().add("spacing-xs");

            return layout;

        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent afterNavigationEvent) {
        UI.getCurrent().getPage().executeJs("return localStorage.getItem(\"userRole\");").then(result -> {
            userRole = result.toJson().replaceAll("[-+.^:,\"]","");

            if(userRole != null && !userRole.equals("admin")){
                gridAddRowButtons.removeAll();
                deleteColumn.getElement().removeFromParent();
            }
        });

        QueryParameters params = afterNavigationEvent.getLocation().getQueryParameters();

        Location location = afterNavigationEvent.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();

        Map<String, List<String>> parametersMap = queryParameters.getParameters();
        selectedValue = parametersMap.get("selected") != null ? parametersMap.get("selected").get(0) : "";

        if(selectedValue != null && selectedValue != ""){
            addButton.setEnabled(true);

            chargesService = chargesFactory.getService(selectedValue);
            chargesList = chargesService.getAll();

            chargesObjList = getChargesObjList(chargesList);

            listOfFilterDropDownOptions = Arrays.asList("vfinbound", "icg", "gsip");

            totalPages = getTotalPagesCount(chargesObjList);

            pageableComponent.configurePaging(totalPages, currentPage);

            setFilterHeaders(listOfFilterDropDownOptions);

            pageViewWithPagination();
        }
    }

    private int getTotalPagesCount(List<ChargesObj> listOfObjects){
        int totalObjects = listOfObjects.size();

        int finalTotalNumberOfPages = (int) Math.ceil(totalObjects/noOfRowsPerPage);

        return finalTotalNumberOfPages;
    }

    private void addEditEventListners(Editor<ChargesObj> chargesEditor, List<TextField> textFieldList){
        textFieldList.forEach(textField -> {
            textField.getElement().addEventListener("keydown", event -> handleKeyDownForEdit(chargesEditor)).setFilter("event.code === 'Enter'");
            textField.getElement().addEventListener("keydown", e -> chargesEditor.cancel()).setFilter("event.code === 'Escape'");
        });
    }

    private void handleKeyDownForEdit(Editor<ChargesObj> chargesEditor){
        ChargesObj chargesObj = chargesBinder.getBean();

        chargesList.forEach(item -> {
            if(item.get_id() == chargesObj.get_id()){
                item.setAction(chargesObj.getAction());
                item.setService(chargesObj.getService());

                if(item.getChargesOaoList().size() > 0){
                    item.getChargesOaoList().forEach(oaoItem ->{
                        if(oaoItem.get_id() == chargesObj.getChild_id()){
                            oaoItem.setAmount(chargesObj.getChild_amount());
                        }
                    });
                }else if(
                        chargesObj.getChild_amount() != null ||
                                chargesObj.getOao() != null
                ) {
                    ChargesOaoDto chargesOaoDto = new ChargesOaoDto();
                    chargesOaoDto.setOao(chargesObj.getOao());
                    chargesOaoDto.setAmount(chargesObj.getChild_amount());
                    List<ChargesOaoDto> chargesOaoDtoList = new ArrayList<ChargesOaoDto>(
                            Arrays.asList(chargesOaoDto)
                    );

                    item.setChargesOaoList(chargesOaoDtoList);
                }

                chargesService.update(item);
                pageViewWithPagination();

                chargesGrid.getDataProvider().refreshAll();

                chargesEditor.save();
                chargesEditor.cancel();
            }
        });
    }

    private GridListDataView<ChargesObj> getDataView(List<ChargesObj> chargesObjList){
        ListDataProvider<ChargesObj> chargesListDataProvider = new ListDataProvider<>(chargesObjList);
        chargesGrid.setDataProvider(chargesListDataProvider);
        GridListDataView<ChargesObj> dataView = chargesGrid.setItems(chargesListDataProvider);

        chargesGrid.getHeaderRows().clear();

        chargesFilter = new ChargesFilter(dataView);

        setFilterHeaders(listOfFilterDropDownOptions);

        return dataView;
    }

    private static class ChargesFilter {
        private final GridListDataView<ChargesObj> dataView;
        private String asset;
        private String type;
        private String charge;
        private String notification;
        private String record_item;
        private String description;
        private String service;
        private String action;
        private String amount;
        private String oao;
        private String oaoAmount;

        public ChargesFilter(GridListDataView<ChargesObj> dataView) {
            this.dataView = dataView;
            this.dataView.addFilter(this::test);
        }

        public void setAsset(String asset) {
            this.asset = asset;
            this.dataView.refreshAll();
        }

        public void setType(String type) {
            this.type = type;
            this.dataView.refreshAll();
        }

        public void setCharge(String charge) {
            this.charge = charge;
            this.dataView.refreshAll();
        }

        public void setNotification(String notification) {
            this.notification = notification;
            this.dataView.refreshAll();
        }

        public void setRecord_item(String record_item) {
            this.record_item = record_item;
            this.dataView.refreshAll();
        }

        public void setDescription(String description) {
            this.description = description;
            this.dataView.refreshAll();
        }

        public void setService(String service) {
            this.service = service;
            this.dataView.refreshAll();
        }

        public void setAction(String action) {
            this.action = action;
            this.dataView.refreshAll();
        }

        public void setAmount(String amount) {
            this.amount = amount;
            this.dataView.refreshAll();
        }

        public void setOao(String oao) {
            this.oao = oao;
            this.dataView.refreshAll();
        }

        public void setOaoAmount(String oaoAmount) {
            this.oaoAmount = oaoAmount;
            this.dataView.refreshAll();
        }

        public boolean test(ChargesObj chargesObj) {
            boolean matchesAsset = matches(chargesObj.getAsset(), asset);
            boolean matchesType = matches(chargesObj.getType(), type);
            boolean matchesCharge = matches(chargesObj.getCharge(), charge);
            boolean matchesNotifications = matches(chargesObj.getNotification(), notification);
            boolean matchesRecord_item = matches(chargesObj.getRecord_item(), record_item);
            boolean matchesDescription = matches(chargesObj.getDescription(), description);
            boolean matchesService = matches(chargesObj.getService(), service);
            boolean matchesAction = matches(chargesObj.getAction(), action);
            boolean matchesAmount = matches(chargesObj.getAmount(), amount);
            boolean matchesOao = matches(chargesObj.getOao(), oao);
            boolean matchesOaoAmount = matches(chargesObj.getChild_amount(), oaoAmount);

            return matchesAsset && matchesType && matchesCharge && matchesNotifications && matchesRecord_item && matchesDescription && matchesService && matchesAction && matchesAmount && matchesOao && matchesOaoAmount;
        }

        private boolean matches(String value, String searchTerm) {
            return searchTerm == null || searchTerm.isEmpty()
                    || value.toLowerCase().contains(searchTerm.toLowerCase());
        }
    }

    private static class ChargesDetailsFormLayout extends FormLayout {
        private final TextField createdField = new TextField("Created");

        public ChargesDetailsFormLayout() {
            Stream.of(createdField).forEach(field -> {
                field.setReadOnly(true);
                add(field);
            });

            setResponsiveSteps(new ResponsiveStep("0", 3));
            setColspan(createdField, 3);
        }

        public void setChargesDto(ChargesObj chargesObj) {
            createdField.setValue(""+chargesObj.getCreated());
        }
    }

    private List<ChargesObj> getChargesObjList(List<ChargesDto> chargesObjList){
        List<ChargesObj> newChargesObjList = new ArrayList<>();

        chargesObjList.forEach(item -> {

            if(item.getChargesOaoList().size() > 0){
                item.getChargesOaoList().forEach(childItem -> {
                    ChargesObj chargesObjWithChild = new ChargesObj(
                            item.get_id(),
                            item.getCreated(),
                            item.getLast_changed(),
                            item.getAsset(),
                            item.getType(),
                            item.getCharge(),
                            item.getNotification(),
                            item.getRecord_item(),
                            item.getDescription(),
                            item.getService(),
                            item.getAction(),
                            item.getAmount(),
                            item.getDiscount(),
                            childItem.get_id(),
                            childItem.getCreated(),
                            childItem.getLast_changed(),
                            childItem.getOao(),
                            childItem.getAmount()
                    );
                    newChargesObjList.add(chargesObjWithChild);
                });
            } else {
                ChargesObj chargesObj = new ChargesObj(
                        item.get_id(),
                        item.getCreated(),
                        item.getLast_changed(),
                        item.getAsset(),
                        item.getType(),
                        item.getCharge(),
                        item.getNotification(),
                        item.getRecord_item(),
                        item.getDescription(),
                        item.getService(),
                        item.getAction(),
                        item.getAmount(),
                        item.getDiscount(),
                        null,
                        null,
                        null,
                        null,
                        null
                );
                newChargesObjList.add(chargesObj);
            }
        });
        return newChargesObjList;
    }
}
