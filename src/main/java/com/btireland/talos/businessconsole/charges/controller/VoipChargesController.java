package com.btireland.talos.businessconsole.charges.controller;

import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.factory.ChargesFactory;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.service.impl.VoipChargesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/voip")
public class VoipChargesController {

    @Autowired
    private VoipChargesServiceImpl voipChargesService;


    @Autowired
    private ChargesFactory chargesFactory;



    @GetMapping("/getAll")
    public List<ChargesDto> getVoipCharges(){
        return voipChargesService.getAll();
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<Optional<ChargesDto>> getVoipCharge(@PathVariable int id){
        Optional<ChargesDto> chargesDto=voipChargesService.getById(id);
        if(chargesDto.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(chargesDto,HttpStatus.OK);
    }

    @PostMapping("/addAll")
    public Optional<List<ChargesDto>> addVoipCharges(@RequestBody List<ChargesDto> chargesDtos) {
        return voipChargesService.saveAll(chargesDtos);
    }

    @PostMapping("/add")
    public Optional<ChargesDto> addVoipCharge(@RequestBody ChargesDto chargesDto) {
        return voipChargesService.save(chargesDto);
    }

    @PutMapping("/update")
    public ChargesDto updateVoipCharge(@RequestBody ChargesDto chargesDto){
        return voipChargesService.update(chargesDto);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteVoipCharge(@PathVariable int id) {
        return voipChargesService.deleteById(id);
    }

    @GetMapping("/getAlltest")
    public Optional<?> gettest(){
        ChargesService chargesService= chargesFactory.getService("VOIP_CHARGES");
        return Optional.ofNullable(chargesService.getAll());
    }
//
////    @PutMapping("/update")
////    public List<VoipCharges> updatetest(){
////        return voipChargesRepository.findAll();
////    }
//    @GetMapping("/getAllOao")
//    public Optional<?> gettestOao(){
//        return Optional.of(voipChargesOaoRepository.findAll());
//    }
}
