package com.btireland.talos.businessconsole.charges.service;

import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface ChargesService {

    List<ChargesDto> getAll();
    Optional<ChargesDto> getById(int id);
    Optional<List<ChargesDto>> saveAll(List<ChargesDto> list);
    Optional<ChargesDto> save(ChargesDto chargesDto);
    ChargesDto update(ChargesDto chargesDto);
    String deleteById(int id);

}
