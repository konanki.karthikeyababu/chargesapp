package com.btireland.talos.businessconsole.charges.controller;

import com.btireland.talos.businessconsole.charges.service.impl.BroadbandChargesServiceImpl;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/broadband")
public class BroadbandChargesController {

    @Autowired
    private BroadbandChargesServiceImpl broadbandChargesService;



    @PostMapping("/add")
    public Optional<ChargesDto> addBroadbandCharges(@RequestBody ChargesDto chargesDto) {
        return broadbandChargesService.save(chargesDto);
    }

    @PostMapping("/addAll")
    public Optional<List<ChargesDto>> addAllBroadbandCharges(@RequestBody List<ChargesDto> chargesDtos) {
        return broadbandChargesService.saveAll(chargesDtos);
    }

    @GetMapping("/getAll")
    public List<ChargesDto> getAllBroadbandCharges() {
        return broadbandChargesService.getAll();
    }

    @GetMapping("/getById/{id}")
    public Optional<ChargesDto> findBroadbandChargesById(@PathVariable int id) {
        return broadbandChargesService.getById(id);
    }

    @PutMapping("/update")
    public ChargesDto updateBroadbandCharges(@RequestBody ChargesDto chargesDto) {
        return broadbandChargesService.update(chargesDto);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteBroadbandCharge(@PathVariable int id) {
        return broadbandChargesService.deleteById(id);
    }

}
