package com.btireland.talos.businessconsole.charges.service.impl;

import com.btireland.talos.businessconsole.charges.entity.WinCharges;
import com.btireland.talos.businessconsole.charges.repository.WinChargesRepository;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import com.btireland.talos.businessconsole.charges.entity.WinChargesOao;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WinChargesServiceImpl implements ChargesService {

    @Autowired
    private WinChargesRepository winChargesRepository;

    @Autowired
    private ModelMapper modelMapper;


    public WinCharges saveWinCharges(WinCharges winCharges) {
        List<WinChargesOao> winChargesOaos = winCharges.getChargesOaoList();
        winChargesOaos.forEach(winChargesOao -> {
            winChargesOao.setWinCharges(winCharges);
        });
        return this.winChargesRepository.save(winCharges);
    }

    public WinCharges getWinChargesById(Integer id) {
        return this.winChargesRepository.findById(id).get();
    }

    public List<WinCharges> getAllWinCharges() {
        return this.winChargesRepository.findAll();
    }

    public List<WinCharges> saveAllWinCharges(List<WinCharges> winCharges) {
        winCharges.forEach(winCharges1 -> {
            List<WinChargesOao> winChargesOaos = winCharges1.getChargesOaoList();
            winChargesOaos.forEach(winChargesOao -> {
                winChargesOao.setWinCharges(winCharges1);
            });
        });
        return this.winChargesRepository.saveAll(winCharges);
    }

    public String deleteWinChargesServiceById(Integer id) {
         this.winChargesRepository.deleteById(id);
         return "item deleted Successfully";
    }


    public String deleteAllWinCharges() {
         this.winChargesRepository.deleteAll();
         return "All item deleted SuccessFully";
    }

    public WinCharges updateWinCharges(WinCharges winCharges) {
        List<WinChargesOao> winChargesOaos = winCharges.getChargesOaoList();
        System.out.println("List: "+winChargesOaos);
        winChargesOaos.forEach(winChargesOao -> {
            winChargesOao.setWinCharges(winCharges);
        });
        return this.winChargesRepository.save(winCharges);
    }

    @Override
    public List<ChargesDto> getAll() {

        List<WinCharges> winCharges=getAllWinCharges();
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for(WinCharges w:winCharges){
            chargesDtos.add(modelMapper.map(w,ChargesDto.class));
        }
        return chargesDtos;
    }

    @Override
    public Optional<ChargesDto> getById(int id) {
        WinCharges winCharges=getWinChargesById(id);
        return Optional.ofNullable(modelMapper.map(winCharges,ChargesDto.class));
    }

    @Override
    public Optional<List<ChargesDto>> saveAll(List<ChargesDto> list) {
        List<WinCharges> winCharges=new ArrayList<>();
        for(ChargesDto chargesDto:list){
            winCharges.add(modelMapper.map(chargesDto,WinCharges.class));
        }
        winCharges=saveAllWinCharges(winCharges);
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for (WinCharges v:winCharges){
            chargesDtos.add(modelMapper.map(v,ChargesDto.class));
        }
        return Optional.ofNullable(chargesDtos);
    }

    @Override
    public Optional<ChargesDto> save(ChargesDto chargesDto) {
        WinCharges winCharges=modelMapper.map(chargesDto,WinCharges.class);
        return Optional.ofNullable(modelMapper.map(saveWinCharges(winCharges), ChargesDto.class));
    }

    @Override
    public ChargesDto update(ChargesDto chargesDto) {
        System.out.println("DTO:"+chargesDto);
        WinCharges winCharges=modelMapper.map(chargesDto,WinCharges.class);
        System.out.println(winCharges);
        return modelMapper.map(updateWinCharges(winCharges),ChargesDto.class);
    }

    @Override
    public String deleteById(int id) {
        return deleteWinChargesServiceById(id);
    }

}
