package com.btireland.talos.businessconsole.charges.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "wlr_charges")
@Audited
@AuditTable(value = "WlrChargesAudit")
public class WlrCharges {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @CreationTimestamp
    private Date created;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;
    private String amount;
    private String discount;

    @OneToMany(mappedBy = "wlrCharges", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<WlrChargesOao> chargesOaoList;

}
