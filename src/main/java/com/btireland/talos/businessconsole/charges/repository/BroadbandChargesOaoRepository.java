package com.btireland.talos.businessconsole.charges.repository;


import com.btireland.talos.businessconsole.charges.entity.BroadbandChargesOao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BroadbandChargesOaoRepository extends JpaRepository<BroadbandChargesOao,Integer> {
}
