package com.btireland.talos.businessconsole.charges.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "win_charges_oao")
@Audited
@AuditTable(value = "WinChargesOaoAudit")
public class WinChargesOao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @CreationTimestamp
    private Date created;
    private String oao;
    private String amount;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "win_charge_id")
    @JsonIgnore
    private WinCharges winCharges;
}
