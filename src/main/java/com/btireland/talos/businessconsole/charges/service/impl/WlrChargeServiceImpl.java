package com.btireland.talos.businessconsole.charges.service.impl;

import com.btireland.talos.businessconsole.charges.entity.WlrCharges;
import com.btireland.talos.businessconsole.charges.entity.WlrChargesOao;
import com.btireland.talos.businessconsole.charges.repository.WlrChargesRepository;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WlrChargeServiceImpl implements ChargesService {

    @Autowired
    private WlrChargesRepository wlrChargesRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<WlrCharges> saveWlrCharges(List<WlrCharges> wlrCharges) {
        return wlrChargesRepository.saveAll(wlrCharges);
    }

    public WlrCharges saveWlrCharge(WlrCharges wlrCharge) {
        List<WlrChargesOao> wlrChargesOaos = wlrCharge.getChargesOaoList();
        for(WlrChargesOao wlrChargesOao : wlrChargesOaos){
            wlrChargesOao.setWlrCharges(wlrCharge);
        }
        return wlrChargesRepository.save(wlrCharge);
    }

    public List<WlrCharges> getAllWlrCharges(){
        return wlrChargesRepository.findAll();
    }

    public Optional<WlrCharges> getWlrCharge(int id){
        return wlrChargesRepository.findById(id);
    }

    public String deleteProduct(int id) {
        wlrChargesRepository.deleteById(id);
        return "product removed !! " + id;
    }

    public WlrCharges updateWlrCharge(WlrCharges wlrCharges){
        List<WlrChargesOao> wlrChargesOaos = wlrCharges.getChargesOaoList();
        wlrChargesOaos.forEach(wlrChargesOao -> {
            wlrChargesOao.setWlrCharges(wlrCharges);
        });
        return wlrChargesRepository.save(wlrCharges);
    }

    @Override
    public List<ChargesDto> getAll() {

        List<WlrCharges> wlrCharges=getAllWlrCharges();
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for(WlrCharges w:wlrCharges){
            chargesDtos.add(modelMapper.map(w,ChargesDto.class));
        }
        return chargesDtos;
    }

    @Override
    public Optional<ChargesDto> getById(int id) {
        Optional<WlrCharges> wlrCharges=getWlrCharge(id);
        return Optional.ofNullable(modelMapper.map(wlrCharges.get(),ChargesDto.class));
    }

    @Override
    public Optional<List<ChargesDto>> saveAll(List<ChargesDto> list) {
        List<WlrCharges> wlrCharges=new ArrayList<>();
        for(ChargesDto chargesDto:list){
            wlrCharges.add(modelMapper.map(chargesDto,WlrCharges.class));
        }
        wlrCharges=saveWlrCharges(wlrCharges);
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for (WlrCharges v:wlrCharges){
            chargesDtos.add(modelMapper.map(v,ChargesDto.class));
        }
        return Optional.ofNullable(chargesDtos);
    }

    @Override
    public Optional<ChargesDto> save(ChargesDto chargesDto) {
        WlrCharges wlrCharges=modelMapper.map(chargesDto,WlrCharges.class);
        return Optional.ofNullable(modelMapper.map(saveWlrCharge(wlrCharges), ChargesDto.class));
    }

    @Override
    public ChargesDto update(ChargesDto chargesDto) {
        WlrCharges wlrCharges=modelMapper.map(chargesDto,WlrCharges.class);
        return modelMapper.map(updateWlrCharge(wlrCharges),ChargesDto.class);
    }

    @Override
    public String deleteById(int id) {
        return deleteProduct(id);
    }
}
