package com.btireland.talos.businessconsole.charges.controller;


import com.btireland.talos.businessconsole.charges.service.impl.WinChargesServiceImpl;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/wincharges")
public class WinChargesController {

    @Autowired
    private WinChargesServiceImpl winChargesService;

    @GetMapping("/getById/{id}")
    public Optional<ChargesDto> getWinChargesByid(@PathVariable Integer id){
        return this.winChargesService.getById(id);
    }

    @GetMapping("/getAll")
    public List<ChargesDto> getAllWinCharges(){

        return this.winChargesService.getAll();
    }

    @PostMapping("/add")
    public Optional<ChargesDto> saveWinCharges(@RequestBody ChargesDto chargesDto){
        return this.winChargesService.save(chargesDto);
    }

    @PostMapping("/addAll")
    public Optional<List<ChargesDto>> saveAllWinCharges(@RequestBody List<ChargesDto> chargesDtos){
        return this.winChargesService.saveAll(chargesDtos);
    }

    @DeleteMapping("/delete/{id}")
    public  String deleteWinCharges(@PathVariable Integer id){
        return this.winChargesService.deleteById(id);
    }

    @DeleteMapping("/deleteAll")
    public String deleleAllWinCharges(){
        return this.winChargesService.deleteAllWinCharges();
    }

    @PutMapping("/update")
    public ChargesDto updateWinCharges(@RequestBody ChargesDto chargesDto){

        System.out.println("Rest: "+chargesDto);
        return this.winChargesService.update(chargesDto);
    }

}
