package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.VoipChargesOao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoipChargesOaoRepository extends JpaRepository<VoipChargesOao,Integer> {
}
