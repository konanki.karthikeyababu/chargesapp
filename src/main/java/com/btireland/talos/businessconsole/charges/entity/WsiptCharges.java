package com.btireland.talos.businessconsole.charges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "wsipt_charges")
@Audited
@AuditTable(value = "WsiptChargesAudit")
public class WsiptCharges {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @CreationTimestamp
    private Date created;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;
    private String product_group;
    @OneToMany(mappedBy = "wsiptCharges",cascade = CascadeType.ALL)
    private List<WsiptChargesOao> chargesOaoList;
}
