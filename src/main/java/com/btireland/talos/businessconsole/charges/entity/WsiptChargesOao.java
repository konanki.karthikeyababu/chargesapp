package com.btireland.talos.businessconsole.charges.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "wsipt_charges_oao")
@Audited
@AuditTable(value = "WsiptChargesOaoAudit")
public class WsiptChargesOao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @CreationTimestamp
    private Date created;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wsipt_charge_id")
    @JsonIgnore
    private WsiptCharges wsiptCharges;
    private String oao;
    private String amount;
}
