package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.WsiptChargesOao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WsiptChargesOaoRepository extends JpaRepository<WsiptChargesOao,Integer> {
}
