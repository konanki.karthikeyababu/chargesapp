package com.btireland.talos.businessconsole.charges.service.impl;

import com.btireland.talos.businessconsole.charges.entity.WsiptCharges;
import com.btireland.talos.businessconsole.charges.entity.WsiptChargesOao;
import com.btireland.talos.businessconsole.charges.repository.WsiptChargesRepository;
import com.btireland.talos.businessconsole.charges.service.ChargesService;
import com.btireland.talos.businessconsole.charges.dto.ChargesDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WsiptChargesServiceImpl implements ChargesService {

    @Autowired
    private WsiptChargesRepository wsiptChargesRepository;

    @Autowired
    private ModelMapper modelMapper;


    public WsiptCharges saveWsiptCharges(WsiptCharges wsiptCharges) {

        List<WsiptChargesOao> wsiptChargesOaos = wsiptCharges.getChargesOaoList();
        wsiptChargesOaos.forEach(wsiptChargesOao -> wsiptChargesOao.setWsiptCharges(wsiptCharges));
        return this.wsiptChargesRepository.save(wsiptCharges);

    }


    public WsiptCharges getWsiptChargesById(Integer id) {

        return this.wsiptChargesRepository.findById(id).get();
    }


    public List<WsiptCharges> getAllWsiptCharges() {
        return this.wsiptChargesRepository.findAll();
    }


    public List<WsiptCharges> saveAllWsiptCharges(List<WsiptCharges> wsiptCharges) {

        wsiptCharges.forEach(wsiptCharges1 -> {
            List<WsiptChargesOao> wsiptChargesOaos = wsiptCharges1.getChargesOaoList();
            wsiptChargesOaos.forEach(wsiptChargesOao -> wsiptChargesOao.setWsiptCharges(wsiptCharges1));
        });
        return this.wsiptChargesRepository.saveAll(wsiptCharges);
    }


    public String deleteWsiptChargesById(Integer id) {
        this.wsiptChargesRepository.deleteById(id);
        return "item deleted successfully";
    }


    public String deleteAllWsiptCharges() {
        this.wsiptChargesRepository.deleteAll();
        return "All item deleted Successfully";
    }


    public WsiptCharges updateWsiptCharges(WsiptCharges wsiptCharges) {
        List<WsiptChargesOao> wsiptChargesOaos = wsiptCharges.getChargesOaoList();
        wsiptChargesOaos.forEach(wsiptChargesOao -> wsiptChargesOao.setWsiptCharges(wsiptCharges));
        return this.wsiptChargesRepository.save(wsiptCharges);
    }

    @Override
    public List<ChargesDto> getAll() {

        List<WsiptCharges> wsiptCharges=getAllWsiptCharges();
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for(WsiptCharges w:wsiptCharges){
            chargesDtos.add(modelMapper.map(w,ChargesDto.class));
        }
        return chargesDtos;
    }

    @Override
    public Optional<ChargesDto> getById(int id) {
        WsiptCharges wsiptCharges=getWsiptChargesById(id);
        return Optional.ofNullable(modelMapper.map(wsiptCharges,ChargesDto.class));
    }

    @Override
    public Optional<List<ChargesDto>> saveAll(List<ChargesDto> list) {
        List<WsiptCharges> wsiptCharges=new ArrayList<>();
        for(ChargesDto chargesDto:list){
            wsiptCharges.add(modelMapper.map(chargesDto,WsiptCharges.class));
        }
        wsiptCharges=saveAllWsiptCharges(wsiptCharges);
        List<ChargesDto> chargesDtos=new ArrayList<>();
        for (WsiptCharges v:wsiptCharges){
            chargesDtos.add(modelMapper.map(v,ChargesDto.class));
        }
        return Optional.ofNullable(chargesDtos);
    }

    @Override
    public Optional<ChargesDto> save(ChargesDto chargesDto) {
        WsiptCharges wsiptCharges=modelMapper.map(chargesDto,WsiptCharges.class);
        return Optional.ofNullable(modelMapper.map(saveWsiptCharges(wsiptCharges), ChargesDto.class));
    }

    @Override
    public ChargesDto update(ChargesDto chargesDto) {
        WsiptCharges wsiptCharges=modelMapper.map(chargesDto,WsiptCharges.class);
        return modelMapper.map(updateWsiptCharges(wsiptCharges),ChargesDto.class);
    }

    @Override
    public String deleteById(int id) {
        return deleteWsiptChargesById(id);
    }
}