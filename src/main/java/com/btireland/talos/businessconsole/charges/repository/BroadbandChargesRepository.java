package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.BroadbandCharges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BroadbandChargesRepository extends JpaRepository<BroadbandCharges,Integer> {
}

