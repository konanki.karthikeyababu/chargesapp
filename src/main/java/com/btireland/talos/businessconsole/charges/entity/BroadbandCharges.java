package com.btireland.talos.businessconsole.charges.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "broadband_charges")
@Audited
@AuditTable(value = "BroadBandChargesAudit")
public class BroadbandCharges {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int _id;
    @CreationTimestamp
    private Date created;
    @UpdateTimestamp
    @NotAudited
    private Date last_changed;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;

    @OneToMany(mappedBy = "broadbandCharges", cascade = CascadeType.ALL)
    private List<BroadbandChargesOao> chargesOaoList;

}
