package com.btireland.talos.businessconsole.charges.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "voip_charges_oao")
@Audited
@AuditTable(value = "VoipChargesOaoAudit")
public class VoipChargesOao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @UpdateTimestamp
    private Date created;
    private String oao;
    private String amount;

    @ManyToOne
    @JoinColumn(name = "voip_charge_id", nullable = false)
    @JsonIgnore
    private VoipCharges voipCharges;
}
