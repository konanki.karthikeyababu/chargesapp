package com.btireland.talos.businessconsole.charges.repository;

import com.btireland.talos.businessconsole.charges.entity.WinCharges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WinChargesRepository extends JpaRepository<WinCharges,Integer> {
}
