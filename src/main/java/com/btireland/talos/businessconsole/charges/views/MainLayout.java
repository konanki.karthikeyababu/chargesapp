package com.btireland.talos.businessconsole.charges.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.theme.lumo.LumoUtility;

import java.util.List;
import java.util.Map;

/**
 * The main view is a top-level placeholder for other views.
 */
public class MainLayout extends AppLayout {
    private H2 viewTitle = new H2("BT");
    private Tab winCharges;
    private Tab wlrCharges;
    private Tab voipCharges;
    private Tab broadbandCharges;
    private Tab wsiptCharges;

    public MainLayout() {
        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
//        toggle.setAriaLabel("Menu toggle");

//        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        viewTitle.addClassNames("bt-title-main-layout");

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        H1 appName = new H1("BT");
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        Header header = new Header(appName);

        Scroller scroller = new Scroller(createNavigation());

        addToDrawer(header, scroller, createFooter());
    }

    private Tabs createNavigation() {
        winCharges = new Tab("Win Charges");
        wlrCharges = new Tab("Wlr Charges");
        voipCharges = new Tab("Voip Charges");
        broadbandCharges = new Tab("Broadband Charges");
        wsiptCharges = new Tab("Wsipt Charges");

        Tabs tabs = new Tabs(false, winCharges, wlrCharges, voipCharges, broadbandCharges, wsiptCharges);

        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);

        tabs.addSelectedChangeListener(event -> {
            String selectedItem = getUrlValue(event.getSelectedTab());

            QueryParameters queryParameters = new QueryParameters(Map.of("selected", List.of(selectedItem)));

            UI.getCurrent().navigate("charges", queryParameters);
        });

        return tabs;
    }

    private String getUrlValue(Tab tab) {
        if (tab == null) {
            return "";
        }
        if (tab.equals(winCharges)) {
            return "WIN_CHARGES";
        } else if (tab.equals(wlrCharges)) {
            return "WLR_CHARGES";
        } else if (tab.equals(voipCharges)) {
            return "VOIP_CHARGES";
        } else if (tab.equals(broadbandCharges)) {
            return "BROADBAND_CHARGES";
        } else if (tab.equals(wsiptCharges)) {
            return "WSIPT_CHARGES";
        } else return "";
    }

    private Footer createFooter() {
        Footer layout = new Footer();

        return layout;
    }
}
