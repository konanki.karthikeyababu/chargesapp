package com.btireland.talos.businessconsole.charges.views.generic;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;
import lombok.Setter;

public class PageableComponent extends HorizontalLayout {
    private final Button firstButton = new Button("First");
    private final Button previousButton = new Button("Previous");
    private final HorizontalLayout pageButtons = new HorizontalLayout();
    private final Button nextButton = new Button("Next");
    private final Button lastButton = new Button("Last");

    @Getter
    @Setter
    private int maxPageCount = 10;

    private int currentPage;
    private int totalPages;

    public PageableComponent() {
        addClassName("pageable-component");
        configureSize();

        configureShortcutButtons();
        configurePaging(totalPages, currentPage);

        add(firstButton, previousButton, pageButtons, nextButton, lastButton);
    }

    private void configureSize() {
        setMaxWidth(2000, Unit.PIXELS);
        setAlignItems(Alignment.CENTER);
    }

    private void configureShortcutButtons() {
        firstButton.setIcon(VaadinIcon.ANGLE_DOUBLE_LEFT.create());
        firstButton.addClickListener(e -> {
            if (isFirstPage()) {
                return;
            }

            fireEvent(new PageChangeEvent(this, 0));
        });

        previousButton.setIcon(VaadinIcon.ANGLE_LEFT.create());
        previousButton.addClickListener(e -> {
            if (isFirstPage()) {
                return;
            }

            fireEvent(new PageChangeEvent(this, currentPage - 1));
        });

        nextButton.setIcon(VaadinIcon.ANGLE_RIGHT.create());
        nextButton.setIconAfterText(true);
        nextButton.addClickListener(e -> {
            if (isLastPage()) {
                return;
            }

            fireEvent(new PageChangeEvent(this, currentPage + 1));
        });

        lastButton.setIcon(VaadinIcon.ANGLE_DOUBLE_RIGHT.create());
        lastButton.setIconAfterText(true);
        lastButton.addClickListener(e -> {
            if (isLastPage()) {
                return;
            }

            fireEvent(new PageChangeEvent(this, totalPages - 1));
        });
    }

    public void configurePaging(int totalPages, int currentPage) {
        this.totalPages = totalPages;
        this.currentPage = currentPage;

        enableShortcutButtons();
        createPageButtons();
    }

    private void enableShortcutButtons() {
        boolean isVisible = isAnyPages();
        firstButton.setVisible(isVisible);
        previousButton.setVisible(isVisible);
        nextButton.setVisible(isVisible);
        lastButton.setVisible(isVisible);

        boolean isBackEnabled = isVisible && !isFirstPage();
        firstButton.setEnabled(isBackEnabled);
        previousButton.setEnabled(isBackEnabled);

        boolean isForwardEnabled = isVisible && !isLastPage();
        nextButton.setEnabled(isForwardEnabled);
        lastButton.setEnabled(isForwardEnabled);
    }

    private void createPageButtons() {
        pageButtons.getChildren().forEach(pageButtons::remove);

        int start = getStartPageNumber();
        int end = getEndPageNumber();

        for (int i = start; i <= end; i++) {
            int pageNumber = i;

            Button pageButton = new Button(String.valueOf(pageNumber + 1));
            pageButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);

            System.out.println("pageNumber,currentPage:::::"+pageNumber+","+currentPage);
            if (pageNumber == currentPage) {
                pageButton.setEnabled(false);
            } else {
                pageButton.addClickListener(e -> fireEvent(new PageChangeEvent(this, pageNumber)));
            }

            pageButtons.add(pageButton);
        }
    }

    public boolean isAnyPages() {
        return totalPages > 0;
    }

    public boolean isFirstPage() {
        System.out.println("isFirstPage:::::"+(currentPage <= 0));
        return currentPage <= 0;
    }

    public boolean isLastPage() {
        return currentPage >= totalPages - 1;
    }

    public int getStartPageNumber() {
        return Math.max(0, Math.min(totalPages - maxPageCount, currentPage - maxPageCount / 2));
    }

    public int getEndPageNumber() {
        return Math.max(0, getStartPageNumber() + Math.min(totalPages, maxPageCount) - 1);
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

    // Events
    @Getter
    public static abstract class PageableEvent extends ComponentEvent<PageableComponent> {
        private final int pageNumber;

        protected PageableEvent(PageableComponent source, int pageNumber) {
            super(source, false);
            this.pageNumber = pageNumber;
        }
    }

    public static class PageChangeEvent extends PageableEvent {
        PageChangeEvent(PageableComponent source, int pageNumber) {
            super(source, pageNumber);
        }
    }
}
