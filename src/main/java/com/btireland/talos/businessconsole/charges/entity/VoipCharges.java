package com.btireland.talos.businessconsole.charges.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "voip_charges")
@Audited
@AuditTable(value = "VoipChargesAudit")
public class VoipCharges {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int _id;
    @UpdateTimestamp
    private Date last_changed;
    @CreationTimestamp
    private Date created;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;

    @OneToMany(mappedBy = "voipCharges", targetEntity = VoipChargesOao.class,cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<VoipChargesOao> chargesOaoList;

}
