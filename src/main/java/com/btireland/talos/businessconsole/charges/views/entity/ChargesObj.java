package com.btireland.talos.businessconsole.charges.views.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

public class ChargesObj {
    private int _id;
    private Date created;
    private Date last_changed;
    private String asset;
    private String type;
    private String charge;
    private String notification;
    private String record_item;
    private String description;
    private String service;
    private String action;
    private String amount;
    private String discount;
    private Integer child_id;
    private Date child_created;
    private Date child_last_changed;
    private String oao;
    private String child_amount;

    public ChargesObj(int _id, Date created, Date last_changed, String asset, String type, String charge, String notification, String record_item, String description, String service, String action, String amount, String discount, Integer child_id, Date child_created, Date child_last_changed, String oao, String child_amount){
        this._id = _id;
        this.created = created;
        this.last_changed = last_changed;
        this.asset = asset;
        this.type = type;
        this.charge = charge;
        this.notification = notification;
        this.record_item = record_item;
        this.description = description;
        this.service = service;
        this.action = action;
        this.amount = amount;
        this.discount = discount;
        this.child_id = child_id;
        this.child_created = child_created;
        this.child_last_changed = child_last_changed;
        this.oao = oao;
        this.child_amount = child_amount;
    }

    public ChargesObj(){}

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLast_changed() {
        return last_changed;
    }

    public void setLast_changed(Date last_changed) {
        this.last_changed = last_changed;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getRecord_item() {
        return record_item;
    }

    public void setRecord_item(String record_item) {
        this.record_item = record_item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Integer getChild_id() {
        return child_id;
    }

    public void setChild_id(Integer child_id) {
        this.child_id = child_id;
    }

    public Date getChild_created() {
        return child_created;
    }

    public void setChild_created(Date child_created) {
        this.child_created = child_created;
    }

    public Date getChild_last_changed() {
        return child_last_changed;
    }

    public void setChild_last_changed(Date child_last_changed) {
        this.child_last_changed = child_last_changed;
    }

    public String getOao() {
        return oao;
    }

    public void setOao(String oao) {
        this.oao = oao;
    }

    public String getChild_amount() {
        return child_amount;
    }

    public void setChild_amount(String child_amount) {
        this.child_amount = child_amount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
